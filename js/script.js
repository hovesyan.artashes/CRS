$(function(){
     $('.plus-button'+'').click(function(){
       var currentId = $(this).data('target');
       console.log($(this).classList);
       if($(this).hasClass('openedButton')){
         $(this).children().css({
           "transform": "rotate(0deg)",
           "transition": 'all 0.5s'
         });
         $('#white-box-'+currentId).remove();
         $(this).removeClass('openedButton')
         setTimeout(function () {
           $('#process-text-'+currentId).removeClass('opened')
         },20)
       }
       else {
         $(this).children().css({
           "transform": "rotate(45deg)",
           "transition": 'all 0.5s'
         });
         setTimeout(function () {
           $('#process-text-'+currentId).addClass('opened')
         },20)
         $('<div class="our-process-white-box" id="white-box-'+currentId+'"></div>').appendTo('#process-text-'+currentId);
         $(this).addClass('openedButton')
       }
     })

     $('.gallery-item').click(function(){
       $('#projectOneCorusel').css('display','block');
       $('#imgGallery').append('<i id="galleryClose" class="float-right fas fa-times fa-3x"></i>')
       $('#galleryClose').click(function(){
         $('#projectOneCorusel').css('display','none');
         $(this).remove();
       })
     })
     var c = 0;
     var b = 0;
     $('body').on('click', '#menuButton', function(){
       if (c%2==0) {
         $(this).css({
           top: 0,
           transition: '1s'
         })
         $('.menu-icon-box').css({
           height: '104px',
           transition: '1s'
         })
         $('#menuButtonText1').hide(500);
         $('#menuButtonText2').show(500);
         $('#menuModal').modal('show');
       }
       else {
         $(this).css({
           top: '71px',
           transition: '1s'
         })
         $('.menu-icon-box').css({
           height: '30',
           transition: '1s'
         })
         $('#menuButtonText2').hide(500);
         $('#menuButtonText1').show(500);
         $('#menuModal').modal('hide');
       }
       c++;
     })
     $('body').on('click', '#langButton', function(){
       if (b%2==0) {
         var rotateButton1 = $('<div class="rotate-button rotate-button-1"></div>')
         var langButton1 = $('<a href="#" class="header-top-button text-center lang-button"></a>')
         var langName1 = $('<h6 class="d-inline-block font-weight-light text-light rotated">ՀԱՅ</h6>')
         var rotateButton2 = $('<div class="rotate-button-2 rotate-button"></div>')
         var langButton2 = $('<button class="header-top-button text-center lang-button"></button>')
         var langName2 = $('<h6 class="d-inline-block font-weight-light text-light rotated">РУС</h6>')
         rotateButton1.appendTo('.lang-button-box');
         langButton1.appendTo(rotateButton1);

         setTimeout(function(){
           rotateButton1.addClass('rotated')

           setTimeout(function(){
             langName1.appendTo(langButton1);
           },150)
         // setTimeout(function(){
         //   rotateButton2.appendTo('.lang-button-box');
         //   langButton2.appendTo(rotateButton2);
         //   setTimeout(function(){
         //     langName2.appendTo(langButton2);
         //     rotateButton2.addClass('rotated')
         //   },200)
         //  },250)
       }, 10);
       }
       else {
         // var rotateButton2 = $('.rotate-button-2');
         var rotateButton1 = $('.rotate-button-1');
         rotateButton1.removeClass('rotated');
         setTimeout(function(){
           rotateButton1.children().html('')
         },150)
         setTimeout(function(){
            rotateButton1.remove();

         },350)
         // setTimeout(function(){
         //    rotateButton2.remove();
         //    rotateButton1.removeClass('rotated');
         //
         // },350)
       }
       b++;
     })

     var lessOpened = false
     $('body').on('click', '#lessOptionButton', function(){
       if (lessOpened) {
         $('#filterLessOptionsBox').hide(500);
         lessOpened = false;
         $(this).html('+ LESS OPTION')
       }
       else {
         $('#filterLessOptionsBox').show(500);
         lessOpened = true;
         $(this).html('- LESS OPTION')
       }

     })

     // $( "#slider-range-area" ).slider({
     //   classes: {
     //     "ui-slider-handle": "cyrcle-hundle",
     //     "ui-slider": "height-4",
     //     "ui-slider-range": "green-range"
     //    },
     //   range: true,
     //   min: 0,
     //   max: 100000,
     //   values: [ 0, 100000 ],
     //   slide: function( event, ui ) {
     //     $( "#amount-area" ).val( ui.values[ 0 ] + " sq. ft - " + ui.values[ 1 ] + " sq. ft" );
     //   }
     // });
     // $( "#amount-area" ).val($( "#slider-range-area" ).slider( "values", 0 ) +
     //   " sq. ft - " + $( "#slider-range-area" ).slider( "values", 1 ) + " sq. ft" );
     //
     //
     //
     // $( "#slider-range-price" ).slider({
     //   classes: {
     //     "ui-slider-handle": "cyrcle-hundle",
     //     "ui-slider": "height-4",
     //     "ui-slider-range": "green-range"
     //    },
     //   range: true,
     //   min: 0,
     //   max: 10000000,
     //   values: [ 0, 10000000 ],
     //   slide: function( event, ui ) {
     //     $( "#amount-price" ).val( ui.values[ 0 ] + " $ - " + ui.values[ 1 ] + " $" );
     //   }
     // });
     // $( "#amount-price" ).val($( "#slider-range-price" ).slider( "values", 0 ) +
     //   " $ - " + $( "#slider-range-price" ).slider( "values", 1 ) + " $" );


     $('#plansModal').on('show.bs.modal', function (e) {
       setTimeout(function() {
         $('.plans-line').css('max-width','820px');
         setTimeout(function() {
           $('.plans-line').css('max-height','500px');
         },800)
       },500)
      })


     $('.plans-view-btn').click(function() {
       $('.plans-view-btn').removeClass('active-view-btn');
       $(this).addClass('active-view-btn');
     })

     $('#plans3dView').click(function() {
       $('.project-one-img-2d').css('display','none');
       $('.project-one-img-3d').css('display','block');
     })
     $('#plans2dView').click(function() {
       $('.project-one-img-2d').css('display','block');
       $('.project-one-img-3d').css('display','none');
     })

     $('.plans-big-btn').click(function() {
       $('.plans-big-btn').removeClass('active-button');
       $(this).addClass('active-button');
     })

     $('.plans-floor-btn').click(function() {
       var targetBtn = this.dataset.target;
       activeBtnLine(targetBtn);
     })

     $('.plans-left-btn').click(function() {
      var target = this.dataset.target;
      setLayout(target);
     })

     $('.project-one-plans-sel').click(function() {
      var target = this.dataset.target;
      $('#plansModal').modal('show');
      setLayout(target);
      activeBtnGreen(target);
     })

     function setLayout(target) {
       $('.layout-box').removeClass('active-layout');
       $('.layout-box').removeClass('active-layout-1');
       var currentFloor = $('.layout-box').eq(target-1);
       currentFloor.addClass('active-layout');
       setTimeout(function() {
         currentFloor.addClass('active-layout-1');
       },100)
     }

     function activeBtnGreen(target) {
       $('.plans-big-btn').removeClass('active-button');
       var currentBtn = $('.plans-big-btn').eq(target-1);
       currentBtn.addClass('active-button');
     }

     function activeBtnLine(target) {
       $('.btn-green-line').remove();
       var currentBtn = $('.plans-floor-btn').eq(target-1);
       currentBtn.append('<div class="btn-green-line"></div>');
     }

       var mapOpened = false
       $('body').on('click', '#mapOpenButton', function(){
         if (mapOpened) {
           $('#listingMapBox').hide(500);
           mapOpened = false;
           $(this).html('Show these properties on a map')
         }
         else {
           $('#listingMapBox').show(500);
           mapOpened = true;
           $(this).html('Hide Map')
         }

       })

         $('.watercolor-back-zoom').css('display','block');
         $('.watercolor-back-zoom').addClass('animated zoomIn');
         setTimeout(function() {
          $('.white-box-roll').css('display','block');
          $('.white-box-roll').addClass('animated rollIn');
          setTimeout(function() {
            $('.watercolor-front-swap').addClass('swaped');
            $('.watercolor-front-zoom').css('display','block');
            $('.watercolor-front-zoom').addClass('animated zoomIn');
          },400)
        },200)

        $('#projectOneCarousel , #projectTwoCarousel').bind('slide.bs.carousel', function (e) {
          $('.watercolor-back-zoom').css('display','none');
          $('.watercolor-back-zoom').removeClass('animated zoomIn');
          $('.white-box-roll').css('display','none');
          $('.white-box-roll').removeClass('animated rollIn');
          $('.watercolor-front-swap').removeClass('swaped');
          $('.watercolor-front-zoom').css('display','none');
          $('.watercolor-front-zoom').removeClass('animated zoomIn');
          $('.watercolor-back-zoom').css('display','block');
          $('.watercolor-back-zoom').addClass('animated zoomIn');
          setTimeout(function() {
           $('.white-box-roll').css('display','block');
           $('.white-box-roll').addClass('animated rollIn');
           setTimeout(function() {
             $('.watercolor-front-swap').addClass('swaped');
             $('.watercolor-front-zoom').css('display','block');
             $('.watercolor-front-zoom').addClass('animated zoomIn');
           },400)
         },200)
        });

        var showPlans = false;
        var showVisual = false;
        $('.visual-floor').click(function() {
          var targetFloor = this.dataset.targetFloor;
          activeBtnLine(targetFloor);
          setLayout(targetFloor);
          showPlans = true;
          $('#projectTwoVisualModal').modal('hide');
          $('#projectTwoVisualModal').on('hidden.bs.modal', function (e) {
            if (showPlans) {
              $('#plansModal').modal('show');
              showPlans = false;
            }
          })
        })

        $('#backToVisual').click(function() {
          $('#plansModal').modal('hide');
          showVisual = true;
          $('#plansModal').on('hidden.bs.modal', function (e) {
            if (showVisual) {
              $('#projectTwoVisualModal').modal('show');
              showVisual = false;
            }
          })
        })



     $(document).ready(function(){
      $("#projectOneOwlCarousel").owlCarousel({
        center: true,
        items:5,
        loop:true,
        nav: false,
        dots: true,
        margin:6,
        dotsContainer: '#carousel-custom-dots'
      });
      $("#detailOwlCarousel").owlCarousel({
        items:5,
        loop:false,
        nav: true,
        dots: false,
        margin:6,
        navText: ["<img src='img/detail-owl-left-icon.png'>","<img src='img/detail-owl-right-icon.png'>"]
      });
      $("#detailFeauturedOwlCarousel").owlCarousel({
        items:1,
        loop:true,
        nav: false,
        dots: true,
        margin:6
      });

    });


})
